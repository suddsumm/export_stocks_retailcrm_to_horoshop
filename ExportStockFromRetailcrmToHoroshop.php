<?php
require 'vendor/autoload.php';

use RetailCrm\Api\Enum\NumericBoolean;
use RetailCrm\Api\Factory\SimpleClientFactory;
use RetailCrm\Api\Model\Filter\Store\InventoryFilterType;
use RetailCrm\Api\Model\Request\Store\InventoriesRequest;

class ExportStockFromRetailcrmToHoroshop
{
    private const RETAILCRM_API_URL = '';
    private const RETAILCRM_API_KEY = '';
    private const HOROSHOP_LOGIN    = '';
    private const HOROSHOP_PASSWORD = '';

    private       $retilcrm_offers_list;
    public        $update_results;


    function __construct()
    {
        self::getRetailcrmStocks();
        self::horoshopUpdateStocks();

        return $this->update_results;
    }

    private function getRetailcrmStocks()
    {
        $client = SimpleClientFactory::createClient(self::RETAILCRM_API_URL, self::RETAILCRM_API_KEY);

        $request                        = new InventoriesRequest();
        $request->filter                = new InventoryFilterType();
        $request->filter->productActive = NumericBoolean::TRUE;
        $request->limit                 = 250; // Сейчас всё умещается на одну страницу но так будет не всегда. 250 это максимум для 1 страницы

        $response = $client->store->inventories($request);

        $totalPageCount = $response->pagination->totalPageCount; // Получаем количество страниц склада

        $current_offers = [];
        for ($current_page = 1; $current_page <= $totalPageCount; $current_page++) { // Для каждой страницы делаем запрос
            $request->page  = $current_page;
            $current_offers = array_merge($current_offers, $client->store->inventories($request)->offers); // Льём всё в current_offers
        }

        $this->retilcrm_offers_list = $current_offers;
    }

    private function horoshopGetAuthToken()
    {
        $headers     = ['Content-Type: application/json'];
        $url         = 'https://rosabella.com.ua/api/auth/';
        $request     = ['login' => self::HOROSHOP_LOGIN, 'password' => self::HOROSHOP_PASSWORD];
        $json_result = self::curlRequest($headers, $url, json_encode($request), 'POST');

        return json_decode($json_result, true)['response']['token'];
    }

    private function horoshopUpdateStocks()
    {
        $headers = ['Content-Type: application/json'];
        $url     = 'https://rosabella.com.ua/api/catalog/importResidues/';
        $request = ['token' => self::horoshopGetAuthToken()];

        // Переделываем в формат Хорошопа
        foreach ($this->retilcrm_offers_list as $product_object) {
            $request['products'][] = ['article' => $product_object->externalId, 'warehouse' => 'office', 'quantity' => $product_object->quantity];
        }

        $this->update_results = self::curlRequest($headers, $url, json_encode($request), 'POST');
    }

    private function curlRequest($headers, $url, $request, $request_type)
    {
        $ch = curl_init(); // Инициализируем cURL
        curl_setopt($ch, CURLOPT_URL, $url); // Обращаемся к $url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_type); // Тип запроса: GET, POST, PUT и т.д.
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // API работает с форматом JSON
        if (isset($request)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request); // Передаём тело запроса
        }
        $result = curl_exec($ch); // Получаем результат
        curl_close($ch); // Завершаем HTTP-соединение cURL

        return $result; // Возвращаем результат функции
    }
}

print_r(new ExportStockFromRetailcrmToHoroshop());
